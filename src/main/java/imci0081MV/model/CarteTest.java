package imci0081MV.model;
import imci0081MV.model.Carte;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CarteTest {
    Carte c;
    Carte c1;
    List<String> autori;
    List<String> autori2;

    @Before
    public void setUp() throws Exception {
        c = new Carte();
        c.setTitlu("abracadabra");

        autori = new ArrayList(Arrays.asList("milan","dingo", "elpha"));
        autori2 = new ArrayList(Arrays.asList("milan","dingo", "elpha"));
        c.setAutori(autori);

        System.out.println("before test ");
    }

    @Test
    public void getAuthor() {
        if(autori.retainAll(c.getAutori()))
            System.out.println("nu merge");
        else System.out.println("merge");
    }

    @Test
    public void setAuthor() {
        autori2.add("Torino");
        c.setAutori(autori2);
        if(autori2.retainAll(c.getAutori()))
            System.out.println("nu merge");
        else System.out.println("merge");
        c.getAutori().forEach(x-> System.out.println(x));
    }


    @After
    public void tearDown() throws Exception {
        c = null;
        System.out.println("after test");

    }


    @Test (expected =NullPointerException .class)
    public void testConstruct(){
        assert  c1==null;
        throw new NullPointerException();
    }

    @Test
    public void getTitlu() {
        assertEquals("titlu=abracadabra","abracadabra",c.getTitlu());
    }

/*



    @Test
    public void setTitlu() {
        c.setTitlu("povestiri");
        assertEquals("povestiri",c.getTitlu());
    }

   // @Test (timeout = 10)
   // public void getEditure(){
   //     try {
     //       Thread.sleep(100);
   //     } catch (InterruptedException e) {
      //      e.printStackTrace();
    //    }
  //  }
//

    @BeforeClass
    public static void setup(){
        System.out.println("before any class");
    }

    @AfterClass

    public static void teardown(){
        System.out.println("after all tests");
    }
*/
}